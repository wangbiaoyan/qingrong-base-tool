<?php

namespace QingrongBase\Tool;


/**
 * 后台权限工具类
 */
class AdminAuthTool
{
    /**
     * 用户登录
     * @param $userName string 用户名
     * @param $userPassword string 密码
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function login($userName, $userPassword)
    {
        $url = env("QINGRONG_BASE_AUTH_URL")."/api/auth/user/login";
        $params = [
            "user_project_id"=>env("QINGRONG_PROJECT_ID"),
            "user_name"=>$userName,
            "user_password"=>$userPassword
        ];
        return BaseTool::postCurlOrigin($url,["query"=>$params],true);
    }

    /**
     * 校验路由权限
     * @param $path string 路径
     * @param $accessToken string token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function checkRouteAutl($path, $accessToken)
    {
        $url = env("QINGRONG_BASE_AUTH_URL")."/api/auth/user/check/rule";
        $header = [
            "Authorization"=>"Bearer ".$accessToken
        ];
        $params = [
            "path"=>$path,
            "project_id"=>env("QINGRONG_PROJECT_ID")
        ];
        return BaseTool::postCurlOrigin($url,["query"=>$params,"headers"=>$header],true);
    }

    /**
     * 用户退出登录
     * @param $accessToken string token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function loginOut($accessToken)
    {
        $url = env("QINGRONG_BASE_AUTH_URL")."/api/auth/user/loginout";
        $header = [
            "Authorization"=>"Bearer ".$accessToken
        ];
        return BaseTool::postCurlOrigin($url,["headers"=>$header],true);
    }

    /**
     * 获取用户信息
     * @param $accessToken string token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getUserInfo($accessToken)
    {
        $url = env("QINGRONG_BASE_AUTH_URL")."/api/auth/user/info";
        $header = [
            "Authorization"=>"Bearer ".$accessToken
        ];
        $params = [
            "project_id"=>env("QINGRONG_PROJECT_ID")
        ];
        return BaseTool::postCurlOrigin($url,["query"=>$params,"headers"=>$header],true);
    }

    /**
     * 更新用户密码
     * @param $userOldPassword string 原密码
     * @param $userPassword string 新密码
     * @param $accessToken string token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function updateUserInfo($userOldPassword,$userNewPassword, $accessToken)
    {
        $url = env("QINGRONG_BASE_AUTH_URL")."/api/auth/user/update/self/password";
        $header = [
            "Authorization"=>"Bearer ".$accessToken
        ];
        $params = [
            "new_password"=>$userNewPassword,
            "old_password"=>$userOldPassword,
        ];
        return BaseTool::postCurlOrigin($url,["query"=>$params,"headers"=>$header],true);
    }
}