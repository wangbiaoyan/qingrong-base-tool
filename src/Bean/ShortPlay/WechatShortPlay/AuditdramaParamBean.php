<?php

namespace QingrongBase\Tool\Bean\ShortPlay\WechatShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;


/**
 * 剧目提审参数
 */
class AuditdramaParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * 剧目id，首次提审不需要填该参数，重新提审时必填
     * @var int $dramaId
     */
    private $dramaId = 0;

    /**
     * 剧名，首次提审时必填，重新提审时根据是否需要修改选填。
     * @var string $name
     */
    private $name = "";

    /**
     * 剧集数目。首次提审时必填， 重新提审时可不填，如要填写也要和第一次提审时一样。
     * @var int $mediaCount
     */
    private $mediaCount = 0;

    /**
     * 剧集媒资media_id列表。首次提审时必填，而且元素个数必须与media_count一致。重新提审时，如果剧集有内容有变化，则需要填写，而且元素个数也必须与 media_count一致
     * @var array $mediaIdList
     */
    private $mediaIdList = [];

    /**
     * 制作方 。首次提审时必填，重新提审时根据是否需要修改选填。
     * @var string $producer
     */
    private $producer = "";

    /**
     * 封面图片临时media_id。首次提审时必填，重新提审时根据是否需要修改选填
     * @var string $coverMaterialId
     */
    private $coverMaterialId = "";

    /**
     * 剧目播放授权材料material_id。如果小程序主体名称和制作方完全一致，则不需要填，否则必填
     * @var string $authorizedMaterialId
     */
    private $authorizedMaterialId = "";

    /**
     * 剧目备案号。首次提审时剧目备案号与网络剧片发行许可证编号二选一。重新提审时根据是否需要修改选填
     * @var string $registrationNumber
     */
    private $registrationNumber = "";

    /**
     * 网络剧片发行许可证编号。首次提审时剧目备案号与网络剧片发行许可证编号二选一。重新提审时根据是否需要修改选填
     * @var string $publishLicense
     */
    private $publishLicense = "";

    /**
     * 网络剧片发行许可证图片，首次提审时如果网络剧片发行许可证编号非空，则该改字段也非空。重新提审时根据是否变化选填
     * @var string $publishLicenseMaterialId
     */
    private $publishLicenseMaterialId = "";

    /**
     * 剧目简介，支持填写100个字符。
     * @var string $description
     */
    private $description = "";

    /**
     * 演员信息，需填写2-5位演员。
     * @var array $actorList
     */
    private $actorList = [];

    /**
     * 剧目资质(0未知 1备案剧 2未备案且小于30万)
     * @var int $qualificationType
     */
    private $qualificationType = 0;

    /**
     * 剧目制作成本（单位：万元）
     * @var int $costOfProduction
     */
    private $costOfProduction = 0;

    /**
     * 其他材料material_id，如涉及互动短剧，请上传完整剧情走线示意图。
     * @var string $otherMaterialMaterialId
     */
    private $otherMaterialMaterialId = "";

    /**
     * 版权保护相关
     * @var array $copyright
     */
    private $copyright = [];

    /**
     * @return array
     */
    public function getCopyright(): array
    {
        return $this->copyright;
    }

    /**
     * @param array $copyright
     */
    public function setCopyright(array $copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return string
     */
    public function getOtherMaterialMaterialId(): string
    {
        return $this->otherMaterialMaterialId;
    }

    /**
     * @param string $otherMaterialMaterialId
     */
    public function setOtherMaterialMaterialId(string $otherMaterialMaterialId)
    {
        $this->otherMaterialMaterialId = $otherMaterialMaterialId;
    }

    /**
     * @return int
     */
    public function getCostOfProduction(): int
    {
        return $this->costOfProduction;
    }

    /**
     * @param int $costOfProduction
     */
    public function setCostOfProduction(int $costOfProduction)
    {
        $this->costOfProduction = $costOfProduction;
    }

    /**
     * 剧目资质文件地址
     * @var string $qualificationCertificateMaterialId
     */
    private $qualificationCertificateMaterialId = "";

    /**
     * 成本配置报告地址
     * @var string $costCommitmentLetterMaterialId
     */
    private $costCommitmentLetterMaterialId = "";

    /**
     * 是否加急审核(1是0否)
     * @var int $expedited
     */
    private $expedited = 0;

    /**
     * @return int
     */
    public function getExpedited(): int
    {
        return $this->expedited;
    }

    /**
     * @param int $expedited
     */
    public function setExpedited(int $expedited)
    {
        $this->expedited = $expedited;
    }

    /**
     * @return int
     */
    public function getQualificationType(): int
    {
        return $this->qualificationType;
    }

    /**
     * @param int $qualificationType
     */
    public function setQualificationType(int $qualificationType)
    {
        $this->qualificationType = $qualificationType;
    }

    /**
     * @return string
     */
    public function getQualificationCertificateMaterialId(): string
    {
        return $this->qualificationCertificateMaterialId;
    }

    /**
     * @param string $qualificationCertificateMaterialId
     */
    public function setQualificationCertificateMaterialId(string $qualificationCertificateMaterialId)
    {
        $this->qualificationCertificateMaterialId = $qualificationCertificateMaterialId;
    }

    /**
     * @return string
     */
    public function getCostCommitmentLetterMaterialId(): string
    {
        return $this->costCommitmentLetterMaterialId;
    }

    /**
     * @param string $costCommitmentLetterMaterialId
     */
    public function setCostCommitmentLetterMaterialId(string $costCommitmentLetterMaterialId)
    {
        $this->costCommitmentLetterMaterialId = $costCommitmentLetterMaterialId;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getActorList(): array
    {
        return $this->actorList;
    }

    /**
     * @param array $actorList
     */
    public function setActorList(array $actorList)
    {
        $this->actorList = $actorList;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getDramaId(): int
    {
        return $this->dramaId;
    }

    /**
     * @param int $dramaId
     */
    public function setDramaId(int $dramaId)
    {
        $this->dramaId = $dramaId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getMediaCount(): int
    {
        return $this->mediaCount;
    }

    /**
     * @param int $mediaCount
     */
    public function setMediaCount(int $mediaCount)
    {
        $this->mediaCount = $mediaCount;
    }

    /**
     * @return array
     */
    public function getMediaIdList(): array
    {
        return $this->mediaIdList;
    }

    /**
     * @param array $mediaIdList
     */
    public function setMediaIdList(array $mediaIdList)
    {
        $this->mediaIdList = $mediaIdList;
    }

    /**
     * @return string
     */
    public function getProducer(): string
    {
        return $this->producer;
    }

    /**
     * @param string $producer
     */
    public function setProducer(string $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @return string
     */
    public function getCoverMaterialId(): string
    {
        return $this->coverMaterialId;
    }

    /**
     * @param string $coverMaterialId
     */
    public function setCoverMaterialId(string $coverMaterialId)
    {
        $this->coverMaterialId = $coverMaterialId;
    }

    /**
     * @return string
     */
    public function getAuthorizedMaterialId(): string
    {
        return $this->authorizedMaterialId;
    }

    /**
     * @param string $authorizedMaterialId
     */
    public function setAuthorizedMaterialId(string $authorizedMaterialId)
    {
        $this->authorizedMaterialId = $authorizedMaterialId;
    }

    /**
     * @return string
     */
    public function getRegistrationNumber(): string
    {
        return $this->registrationNumber;
    }

    /**
     * @param string $registrationNumber
     */
    public function setRegistrationNumber(string $registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;
    }

    /**
     * @return string
     */
    public function getPublishLicense(): string
    {
        return $this->publishLicense;
    }

    /**
     * @param string $publishLicense
     */
    public function setPublishLicense(string $publishLicense)
    {
        $this->publishLicense = $publishLicense;
    }

    /**
     * @return string
     */
    public function getPublishLicenseMaterialId(): string
    {
        return $this->publishLicenseMaterialId;
    }

    /**
     * @param string $publishLicenseMaterialId
     */
    public function setPublishLicenseMaterialId(string $publishLicenseMaterialId)
    {
        $this->publishLicenseMaterialId = $publishLicenseMaterialId;
    }
}