<?php

namespace QingrongBase\Tool\Bean\ShortPlay\WechatShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class ListmediaParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * 剧目ID
     * @var int $dramaId
     */
    private $dramaId = 0;

    /**
     * 媒资文件名
     * @var string $mediaName
     */
    private $mediaName = "";

    /**
     * 开始时间
     * @var int $startTime
     */
    private $startTime = 0;

    /**
     * 结束时间
     * @var int $endTime
     */
    private $endTime = 0;

    /**
     * 每页数量
     * @var int $limit
     */
    private $limit = 0;

    /**
     * 偏移量
     * @var int $offset
     */
    private $offset = 0;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getDramaId(): int
    {
        return $this->dramaId;
    }

    /**
     * @param int $dramaId
     */
    public function setDramaId(int $dramaId)
    {
        $this->dramaId = $dramaId;
    }

    /**
     * @return string
     */
    public function getMediaName(): string
    {
        return $this->mediaName;
    }

    /**
     * @param string $mediaName
     */
    public function setMediaName(string $mediaName)
    {
        $this->mediaName = $mediaName;
    }

    /**
     * @return int
     */
    public function getStartTime(): int
    {
        return $this->startTime;
    }

    /**
     * @param int $startTime
     */
    public function setStartTime(int $startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return int
     */
    public function getEndTime(): int
    {
        return $this->endTime;
    }

    /**
     * @param int $endTime
     */
    public function setEndTime(int $endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset)
    {
        $this->offset = $offset;
    }
}