<?php

namespace QingrongBase\Tool\Bean\ShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class PullUploadParamBean extends ToolBaseBean
{
    /**
     * 视频地址
     * @var $videoUrl string
     */
    private $videoUrl = "";

    /**
     * 视频格式
     * @var string $videoFormat
     */
    private $videoFormat = "";

    /**
     * 视频名称
     * @var string $videoName
     */
    private $videoName = "";

    /**
     * 视频封面地址
     * @var string $coverUrl
     */
    private $coverUrl = "";

    /**
     * 来源上下文，会在上传完成事件中透传给开发者。
     * @var string $sourceContext
     */
    private $sourceContext="";

    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * 抖音appid
     * @var string $douyinAppId
     */
    private $douyinAppId = "";

    /**
     * @return string
     */
    public function getDouyinAppId(): string
    {
        return $this->douyinAppId;
    }

    /**
     * @param string $douyinAppId
     */
    public function setDouyinAppId(string $douyinAppId)
    {
        $this->douyinAppId = $douyinAppId;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }


    /**
     * @return string
     */
    public function getVideoUrl(): string
    {
        return $this->videoUrl;
    }

    /**
     * @param string $videoUrl
     */
    public function setVideoUrl(string $videoUrl)
    {
        $this->videoUrl = $videoUrl;
    }

    /**
     * @return string
     */
    public function getVideoFormat(): string
    {
        return $this->videoFormat;
    }

    /**
     * @param string $videoFormat
     */
    public function setVideoFormat(string $videoFormat)
    {
        $this->videoFormat = $videoFormat;
    }

    /**
     * @return string
     */
    public function getVideoName(): string
    {
        return $this->videoName;
    }

    /**
     * @param string $videoName
     */
    public function setVideoName(string $videoName)
    {
        $this->videoName = $videoName;
    }

    /**
     * @return string
     */
    public function getCoverUrl(): string
    {
        return $this->coverUrl;
    }

    /**
     * @param string $coverUrl
     */
    public function setCoverUrl(string $coverUrl)
    {
        $this->coverUrl = $coverUrl;
    }

    /**
     * @return string
     */
    public function getSourceContext(): string
    {
        return $this->sourceContext;
    }

    /**
     * @param string $sourceContext
     */
    public function setSourceContext(string $sourceContext)
    {
        $this->sourceContext = $sourceContext;
    }
}