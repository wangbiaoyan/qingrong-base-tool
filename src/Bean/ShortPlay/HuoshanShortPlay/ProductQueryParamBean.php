<?php

namespace QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class ProductQueryParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * appId
     * @var string $maAppId
     */
    private $maAppId = "";

    /**
     * 查询类型 1-查询小程序下所有短剧信息 2-查询一个短剧所有版本信息 3-查询一个短剧某个版本下所有剧集信息
     * @var int $queryType
     */
    private $queryType = 1;

    /**
     * 查询小程序下所有短剧信息所需参数
     * @var array $batchQuery
     */
    private $batchQuery = [];

    /**
     * 查询一个短剧所有版本信息所需参数
     * @var array $singleQuery
     */
    private $singleQuery = [];

    /**
     * 查询一个短剧某个版本下所有剧集信息所需参数
     * @var array $detailQuery
     */
    private $detailQuery = [];

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getMaAppId(): string
    {
        return $this->maAppId;
    }

    /**
     * @param string $maAppId
     */
    public function setMaAppId(string $maAppId)
    {
        $this->maAppId = $maAppId;
    }

    /**
     * @return int
     */
    public function getQueryType(): int
    {
        return $this->queryType;
    }

    /**
     * @param int $queryType
     */
    public function setQueryType(int $queryType)
    {
        $this->queryType = $queryType;
    }

    /**
     * @return array
     */
    public function getBatchQuery(): array
    {
        return $this->batchQuery;
    }

    /**
     * @param array $batchQuery
     */
    public function setBatchQuery(array $batchQuery)
    {
        $this->batchQuery = $batchQuery;
    }

    /**
     * @return array
     */
    public function getSingleQuery(): array
    {
        return $this->singleQuery;
    }

    /**
     * @param array $singleQuery
     */
    public function setSingleQuery(array $singleQuery)
    {
        $this->singleQuery = $singleQuery;
    }

    /**
     * @return array
     */
    public function getDetailQuery(): array
    {
        return $this->detailQuery;
    }

    /**
     * @param array $detailQuery
     */
    public function setDetailQuery(array $detailQuery)
    {
        $this->detailQuery = $detailQuery;
    }
}