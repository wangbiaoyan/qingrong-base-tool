<?php

namespace QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class ProductModifyParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * 剧目ID
     * @var int $ablumId
     */
    private $albumId = 0;

    /**
     * 剧集信息 单次（需要小于100），总数没有限制
     * @var array $episodeInfoList
     */
    private $episodeInfoList = [];

    /**
     * 短剧信息，会覆盖之前的短剧信息
     * @var array $albumInfo
     */
    private $albumInfo = [];

    /**
     * appId
     * @var string $maAppId
     */
    private $maAppId = "";

    /**
     * @return string
     */
    public function getMaAppId(): string
    {
        return $this->maAppId;
    }

    /**
     * @param string $maAppId
     */
    public function setMaAppId(string $maAppId)
    {
        $this->maAppId = $maAppId;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getAlbumId(): int
    {
        return $this->albumId;
    }

    /**
     * @param int $albumId
     */
    public function setAlbumId(int $albumId)
    {
        $this->albumId = $albumId;
    }

    /**
     * @return array
     */
    public function getEpisodeInfoList(): array
    {
        return $this->episodeInfoList;
    }

    /**
     * @param array $episodeInfoList
     */
    public function setEpisodeInfoList(array $episodeInfoList)
    {
        $this->episodeInfoList = $episodeInfoList;
    }

    /**
     * @return array
     */
    public function getAlbumInfo(): array
    {
        return $this->albumInfo;
    }

    /**
     * @param array $albumInfo
     */
    public function setAlbumInfo(array $albumInfo)
    {
        $this->albumInfo = $albumInfo;
    }
}