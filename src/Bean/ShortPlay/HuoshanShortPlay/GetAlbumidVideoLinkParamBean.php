<?php

namespace QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class GetAlbumidVideoLinkParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * appId
     * @var string $maAppId
     */
    private $maAppId = "";

    /**
     * 剧目ID
     * @var int $albumId
     */
    private $albumId = 0;

    /**
     * 剧集ID
     * @var int $episodeId
     */
    private $episodeId = 0;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getMaAppId(): string
    {
        return $this->maAppId;
    }

    /**
     * @param string $maAppId
     */
    public function setMaAppId(string $maAppId)
    {
        $this->maAppId = $maAppId;
    }

    /**
     * @return int
     */
    public function getAlbumId(): int
    {
        return $this->albumId;
    }

    /**
     * @param int $albumId
     */
    public function setAlbumId(int $albumId)
    {
        $this->albumId = $albumId;
    }

    /**
     * @return int
     */
    public function getEpisodeId(): int
    {
        return $this->episodeId;
    }

    /**
     * @param int $episodeId
     */
    public function setEpisodeId(int $episodeId)
    {
        $this->episodeId = $episodeId;
    }
}