<?php

namespace QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class AlbumidOpLineParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * 剧目ID
     * @var int $albumId
     */
    private $albumId = 0;

    /**
     * 操作类型 1-查询 2-修改
     * @var int $operate
     */
    private $operate = 1;

    /**
     * 版本
     * @var int $version
     */
    private $version = 0;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return int
     */
    public function getAlbumId(): int
    {
        return $this->albumId;
    }

    /**
     * @param int $albumId
     */
    public function setAlbumId(int $albumId)
    {
        $this->albumId = $albumId;
    }

    /**
     * @return int
     */
    public function getOperate(): int
    {
        return $this->operate;
    }

    /**
     * @param int $operate
     */
    public function setOperate(int $operate)
    {
        $this->operate = $operate;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }
}