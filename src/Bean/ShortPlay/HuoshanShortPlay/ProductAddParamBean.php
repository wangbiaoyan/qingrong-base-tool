<?php

namespace QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay;

use QingrongBase\Tool\Bean\ToolBaseBean;

class ProductAddParamBean extends ToolBaseBean
{
    /**
     * token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * appId
     * @var string $maAppId
     */
    private $maAppId = "";

    /**
     * 短剧标题
     * @var string $title
     */
    private $title = "";

    /**
     * 总集数
     * @var int $seqNum
     */
    private $seqNum = 0;

    /**
     * 封面列表
     * @var array $coverList
     */
    private $coverList = [];

    /**
     * 发行年份
     * @var int $year
     */
    private $year = 0;

    /**
     * 短剧更新状态 1-未上映 2-更新中 3-已完结
     * @var int $albumStatus
     */
    private $albumStatus = 1;

    /**
     * 短剧推荐语（12汉字以内
     * @var string $recommendation
     */
    private $recommendation = "";

    /**
     * 短剧简介（200汉字以内
     * @var string $desp
     */
    private $desp = "";

    /**
     * 短剧类目标签（1-3个）
     * @var array $tagList
     */
    private $tagList = [];

    /**
     * 资质状态 1-未报审2-报审通过3-报审不通过4-不建议报审
     * @var int $qualification
     */
    private $qualification = 0;

    /**
     * 普通备案号
     * @var string $ordinaryRecordNum
     */
    private $ordinaryRecordNum = "";

    /**
     * 重点备案号
     * @var string $keyRecordNum
     */
    private $keyRecordNum = "";

    /**
     * 备案审核信息
     * @var array
     */
    private $recordAuditInfo = [];

    /**
     * @return array
     */
    public function getRecordAuditInfo(): array
    {
        return $this->recordAuditInfo;
    }

    /**
     * @param array $recordAuditInfo
     */
    public function setRecordAuditInfo(array $recordAuditInfo)
    {
        $this->recordAuditInfo = $recordAuditInfo;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getMaAppId(): string
    {
        return $this->maAppId;
    }

    /**
     * @param string $maAppId
     */
    public function setMaAppId(string $maAppId)
    {
        $this->maAppId = $maAppId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getSeqNum(): int
    {
        return $this->seqNum;
    }

    /**
     * @param int $seqNum
     */
    public function setSeqNum(int $seqNum)
    {
        $this->seqNum = $seqNum;
    }

    /**
     * @return array
     */
    public function getCoverList(): array
    {
        return $this->coverList;
    }

    /**
     * @param array $coverList
     */
    public function setCoverList(array $coverList)
    {
        $this->coverList = $coverList;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getAlbumStatus(): int
    {
        return $this->albumStatus;
    }

    /**
     * @param int $albumStatus
     */
    public function setAlbumStatus(int $albumStatus)
    {
        $this->albumStatus = $albumStatus;
    }

    /**
     * @return string
     */
    public function getRecommendation(): string
    {
        return $this->recommendation;
    }

    /**
     * @param string $recommendation
     */
    public function setRecommendation(string $recommendation)
    {
        $this->recommendation = $recommendation;
    }

    /**
     * @return string
     */
    public function getDesp(): string
    {
        return $this->desp;
    }

    /**
     * @param string $desp
     */
    public function setDesp(string $desp)
    {
        $this->desp = $desp;
    }

    /**
     * @return array
     */
    public function getTagList(): array
    {
        return $this->tagList;
    }

    /**
     * @param array $tagList
     */
    public function setTagList(array $tagList)
    {
        $this->tagList = $tagList;
    }

    /**
     * @return int
     */
    public function getQualification(): int
    {
        return $this->qualification;
    }

    /**
     * @param int $qualification
     */
    public function setQualification(int $qualification)
    {
        $this->qualification = $qualification;
    }

    /**
     * @return string
     */
    public function getOrdinaryRecordNum(): string
    {
        return $this->ordinaryRecordNum;
    }

    /**
     * @param string $ordinaryRecordNum
     */
    public function setOrdinaryRecordNum(string $ordinaryRecordNum)
    {
        $this->ordinaryRecordNum = $ordinaryRecordNum;
    }

    /**
     * @return string
     */
    public function getKeyRecordNum(): string
    {
        return $this->keyRecordNum;
    }

    /**
     * @param string $keyRecordNum
     */
    public function setKeyRecordNum(string $keyRecordNum)
    {
        $this->keyRecordNum = $keyRecordNum;
    }
}