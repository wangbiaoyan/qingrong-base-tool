<?php

namespace QingrongBase\Tool\ShortPlay;

use QingrongBase\Tool\BaseTool;
use QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay\AlbumidOpLineParamBean;
use QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay\GetAlbumidVideoLinkParamBean;
use QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay\ProductAddParamBean;
use QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay\ProductModifyParamBean;
use QingrongBase\Tool\Bean\ShortPlay\HuoshanShortPlay\ProductQueryParamBean;
use QingrongBase\Tool\Bean\ShortPlay\PullUploadParamBean;

class HuoshanShortPlay implements ShortPlayInterface
{
    /**
     * 上传视频
     * @author rsx
     * @date 2023-06-27 10:43
     */
    public function pullUpload(PullUploadParamBean $pullUploadParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/resource/upload/';
        $options['query'] = [
            "resource_type" => 1,
            "ma_app_id"     => $pullUploadParamBean->getDouyinAppId(),
            "video_meta"    => [
                "url"          => $pullUploadParamBean->getVideoUrl(),
                "title"        => $pullUploadParamBean->getVideoName(),
                "format"       => $pullUploadParamBean->getVideoFormat(),
                "use_dy_cloud" => true
            ]
        ];
        $options['headers'] = [ 'access-token' => $pullUploadParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 查询视频上传状态
     * @author rsx
     * @date 2023-06-27 10:44
     */
    public function getUploadStatusV1($taskId, $accessToken, $maAppId)
    {
        $url = "https://open.douyin.com/api/playlet/v2/video/query/";
        $options['query'] = [
            "video_id_type" => 1,
            "ma_app_id"     => $maAppId,
            'open_video_id' => $taskId,
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 查询视频上传状态
     * @author rsx
     * @date 2023-06-27 10:44
     */
    public function getUploadStatus($taskId, $accessToken)
    {
        $url = "https://open.douyin.com/api/playlet/v2/video/query/";
        $options['query'] = [
            "video_id_type" => 1,
            "ma_app_id"     => "",
            'open_video_id' => $taskId,
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 图片上传
     * @author rsx
     * @date 2023-06-27 10:44
     */
    public function imgUpload($picUrl, $accessToken, $maAppId)
    {
        $url = "https://open.douyin.com/api/playlet/v2/resource/upload/";
        $options['query'] = [
            "resource_type" => 2,
            "ma_app_id"     => $maAppId,
            "image_meta"    => [
                "url" => $picUrl,
            ]
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 查询审核模板
     * @author rsx
     * @date 2023-06-27 10:44
     */
    public function productTemplateQuery($accessToken)
    {
        $url = "https://open.douyin.com/api/product/v1/product_template_query";
        $options['query'] = [
            'biz_line'     => 4,
            'product_type' => 3,
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 短剧信息新增
     * @author rsx
     * @date 2023-06-27 16:23
     */
    public function productAdd(ProductAddParamBean $productAddParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/video/create/';
        $options['query'] = [
            'ma_app_id'  => $productAddParamBean->getMaAppId(),
            'album_info' => [
                "title"          => $productAddParamBean->getTitle(),
                "seq_num"        => $productAddParamBean->getSeqNum(),
                "cover_list"     => $productAddParamBean->getCoverList(),
                "year"           => $productAddParamBean->getYear(),
                "album_status"   => $productAddParamBean->getAlbumStatus(),
                "recommendation" => $productAddParamBean->getRecommendation(),
                "desp"           => $productAddParamBean->getDesp(),
                "tag_list"       => $productAddParamBean->getTagList(),
                "qualification"  => $productAddParamBean->getQualification(),
//                "record_info"    => [],
                "record_audit_info" => $productAddParamBean->getRecordAuditInfo()
            ]
        ];
//        if ($productAddParamBean->getKeyRecordNum()) {
//            $options['query']["album_info"]["record_info"]["key_record_num"] = $productAddParamBean->getKeyRecordNum();
//        }
//        if ($productAddParamBean->getOrdinaryRecordNum()) {
//            $options['query']["album_info"]["record_info"]["ordinary_record_num"] = $productAddParamBean->getOrdinaryRecordNum();
//        }
        $options['headers'] = [ 'access-token' => $productAddParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 短剧信息修改
     * @author rsx
     * @date 2023-06-27 16:23
     */
    public function productModify(ProductModifyParamBean $productModifyParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/video/edit/';
        $options['query'] = [
            'ma_app_id' => $productModifyParamBean->getMaAppId(),
            "album_id"  => $productModifyParamBean->getAlbumId()
        ];

        if ($productModifyParamBean->getEpisodeInfoList()) {
            $options["query"]["episode_info_list"] = $productModifyParamBean->getEpisodeInfoList();
        }
        if ($productModifyParamBean->getAlbumInfo()) {
            $options["query"]["album_info"] = $productModifyParamBean->getAlbumInfo();
        }
        $options['headers'] = [ 'access-token' => $productModifyParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 短剧查询
     * @param ProductQueryParamBean $productQueryParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productQuery(ProductQueryParamBean $productQueryParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/album/fetch/';
        $options['query'] = [
            'ma_app_id'  => $productQueryParamBean->getMaAppId(),
            "query_type" => $productQueryParamBean->getQueryType(),
        ];
        if ($productQueryParamBean->getBatchQuery()) {
            $options["query"]["batch_query"] = $productQueryParamBean->getBatchQuery();
        }
        if ($productQueryParamBean->getSingleQuery()) {
            $options["query"]["single_query"] = $productQueryParamBean->getSingleQuery();
        }
        if ($productQueryParamBean->getDetailQuery()) {
            $options["query"]["detail_query"] = $productQueryParamBean->getDetailQuery();
        }
        $options['headers'] = [ 'access-token' => $productQueryParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 短剧提审
     * @param $accessToken
     * @param $maAppId
     * @param $albumId
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function auditdrama($accessToken, $maAppId, $albumId)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/video/review/';
        $options['query'] = [
            'ma_app_id' => $maAppId,
            "album_id"  => $albumId
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 短剧授权
     * @author rsx
     * @date 2023-06-27 15:56
     */
    public function updateAuthAppids($appIdList, $albumId, $accessToken, $maAppId)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/auth/authorize/';
        $options['query'] = [
            'ma_app_id'   => $maAppId,
            'app_id_list' => $appIdList,
            'album_id'    => $albumId,
            'increment'   => true
        ];
        $options['headers'] = [ 'access-token' => $accessToken ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 获取播放链接
     * @param GetAlbumidVideoLinkParamBean $getAlbumidVideoLinkParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAlbumidVideoLink(GetAlbumidVideoLinkParamBean $getAlbumidVideoLinkParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/video/play_info/';
        $options['query'] = [
            'ma_app_id'  => $getAlbumidVideoLinkParamBean->getMaAppId(),
            'album_id'   => $getAlbumidVideoLinkParamBean->getAlbumId(),
            'episode_id' => $getAlbumidVideoLinkParamBean->getEpisodeId()
        ];
        $options['headers'] = [ 'access-token' => $getAlbumidVideoLinkParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }

    /**
     * 获取播放链接
     * @author rsx
     * @date 2023-06-27 16:23
     */
    public function albumidOnLine(AlbumidOpLineParamBean $albumidOpLineParamBean)
    {
        $url = 'https://open.douyin.com/api/playlet/v2/album/online/';
        $options['query'] = [
            'album_id' => $albumidOpLineParamBean->getAlbumId(),
            'operate'  => $albumidOpLineParamBean->getOperate()
        ];
        if ($albumidOpLineParamBean->getVersion()) {
            $options["query"]["version"] = $albumidOpLineParamBean->getVersion();
        }
        $options['headers'] = [ 'access-token' => $albumidOpLineParamBean->getAccessToken() ];
        return BaseTool::postCurl($url, $options, true);
    }
}