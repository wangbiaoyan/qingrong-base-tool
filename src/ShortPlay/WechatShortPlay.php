<?php

namespace QingrongBase\Tool\ShortPlay;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;
use QingrongBase\Tool\BaseTool;
use QingrongBase\Tool\Bean\ShortPlay\PullUploadParamBean;
use QingrongBase\Tool\Bean\ShortPlay\WechatShortPlay\AuditdramaParamBean;
use QingrongBase\Tool\Bean\ShortPlay\WechatShortPlay\ListmediaParamBean;

class WechatShortPlay implements ShortPlayInterface
{
    /**
     * 上传视频
     * @param PullUploadParamBean $pullUploadParamBean
     * @return mixed|void
     */
    public function pullUpload(PullUploadParamBean $pullUploadParamBean)
    {
        // TODO: Implement pullUpload() method.
        $url = "https://api.weixin.qq.com/wxa/sec/vod/pullupload?access_token=" . $pullUploadParamBean->getAccessToken();
        $params = [
            "media_name" => $pullUploadParamBean->getVideoName(),
            "media_url"  => $pullUploadParamBean->getVideoUrl()
        ];
        if ($pullUploadParamBean->getCoverUrl()) {
            $params["cover_url"] = $pullUploadParamBean->getCoverUrl();
        }

        if ($pullUploadParamBean->getSourceContext()) {
            $params["source_context"] = $pullUploadParamBean->getSourceContext();
        }

        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 查询任务状态
     * @param $taskId
     * @param $accessToken
     * @return mixed|void
     */
    public function getUploadStatus($taskId, $accessToken)
    {
        // TODO: Implement getUploadStatus() method.
        $url = "https://api.weixin.qq.com/wxa/sec/vod/gettask?access_token=" . $accessToken;
        $params = [
            "task_id" => $taskId
        ];
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 获取媒资播放链接
     * @param $mediaId
     * @param $expireTime
     * @param $accessToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getmedialink($mediaId, $expireTime, $accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/getmedialink?access_token=" . $accessToken;

        $params = [
            "media_id" => $mediaId,
            "t"        => $expireTime
        ];

        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 删除媒资
     * @param $mediaId
     * @param $accessToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deletemedia($mediaId, $accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/deletemedia?access_token=" . $accessToken;

        $params = [
            "media_id" => $mediaId,
        ];

        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 获取媒资列表
     * @param ListmediaParamBean $listmediaParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listmedia(ListmediaParamBean $listmediaParamBean)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/listmedia?access_token=" . $listmediaParamBean->getAccessToken();
        $params = [];
        if ($listmediaParamBean->getDramaId()) {
            $params["drama_id"] = $listmediaParamBean->getDramaId();
        }
        if ($listmediaParamBean->getMediaName()) {
            $params["media_name"] = $listmediaParamBean->getMediaName();
        }
        if ($listmediaParamBean->getStartTime()) {
            $params["start_time"] = $listmediaParamBean->getStartTime();
        }
        if ($listmediaParamBean->getEndTime()) {
            $params["end_time"] = $listmediaParamBean->getEndTime();
        }
        if ($listmediaParamBean->getLimit()) {
            $params["limit"] = $listmediaParamBean->getLimit();
        }
        if ($listmediaParamBean->getOffset()) {
            $params["offset"] = $listmediaParamBean->getOffset();
        }
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 获取剧目列表
     * @param $limit
     * @param $offset
     * @param $accessToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listdramas($limit, $offset, $accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/listdramas?access_token=" . $accessToken;
        $params = [
            "limit"  => $limit,
            "offset" => $offset
        ];
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 获取剧目信息
     * @param $dramaId int 剧目ID
     * @param $accessToken string token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getdrama($dramaId, $accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/getdrama?access_token=" . $accessToken;
        $params = [
            "drama_id" => $dramaId
        ];
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 获取媒资详细信息
     * @param $mediaId
     * @param $accessToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getmedia($mediaId, $accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/getmedia?access_token=" . $accessToken;
        $params = [
            "media_id" => $mediaId
        ];
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 剧目提审
     * @param AuditdramaParamBean $auditdramaParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function auditdrama(AuditdramaParamBean $auditdramaParamBean)
    {
        $url = "https://api.weixin.qq.com/wxa/sec/vod/auditdrama?access_token=" . $auditdramaParamBean->getAccessToken();
        $params = [];

        if ($auditdramaParamBean->getDramaId()) {
            $params["drama_id"] = $auditdramaParamBean->getDramaId();
        }
        if ($auditdramaParamBean->getName()) {
            $params["name"] = $auditdramaParamBean->getName();
        }
        if ($auditdramaParamBean->getMediaCount()) {
            $params["media_count"] = $auditdramaParamBean->getMediaCount();
        }
        if ($auditdramaParamBean->getMediaIdList()) {
            $params["media_id_list"] = $auditdramaParamBean->getMediaIdList();
        }
        if ($auditdramaParamBean->getProducer()) {
            $params["producer"] = $auditdramaParamBean->getProducer();
        }
        if ($auditdramaParamBean->getCoverMaterialId()) {
            $params["cover_material_id"] = $auditdramaParamBean->getCoverMaterialId();
        }
        if ($auditdramaParamBean->getAuthorizedMaterialId()) {
            $params["authorized_material_id"] = $auditdramaParamBean->getAuthorizedMaterialId();
        }
        if ($auditdramaParamBean->getRegistrationNumber()) {
            $params["registration_number"] = $auditdramaParamBean->getRegistrationNumber();
        }
        if ($auditdramaParamBean->getPublishLicense()) {
            $params["publish_license"] = $auditdramaParamBean->getPublishLicense();
        }
        if ($auditdramaParamBean->getPublishLicenseMaterialId()) {
            $params["publish_license_material_id"] = $auditdramaParamBean->getPublishLicenseMaterialId();
        }
        if ($auditdramaParamBean->getDescription()) {
            $params["description"] = $auditdramaParamBean->getDescription();
        }
        if ($auditdramaParamBean->getActorList()) {
            $params["actor_list"] = $auditdramaParamBean->getActorList();
        }

        if ($auditdramaParamBean->getQualificationType()) {
            $params["qualification_type"] = $auditdramaParamBean->getQualificationType();
        }
        if ($auditdramaParamBean->getQualificationCertificateMaterialId()) {
            $params["qualification_certificate_material_id"] = $auditdramaParamBean->getQualificationCertificateMaterialId();
        }
        if ($auditdramaParamBean->getCostCommitmentLetterMaterialId()) {
            $params["cost_commitment_letter_material_id"] = $auditdramaParamBean->getCostCommitmentLetterMaterialId();
        }
        if ($auditdramaParamBean->getExpedited()) {
            $params["expedited"] = $auditdramaParamBean->getExpedited();
        }
        if ($auditdramaParamBean->getCostOfProduction()) {
            $params["cost_of_production"] = $auditdramaParamBean->getCostOfProduction();
        }
        if ($auditdramaParamBean->getOtherMaterialMaterialId()) {
            $params["other_material_material_id"] = $auditdramaParamBean->getOtherMaterialMaterialId();
        }
        if ($auditdramaParamBean->getCopyright()) {
            $params["copyright"] = $auditdramaParamBean->getCopyright();
        }
        return BaseTool::postCurl($url, [ "query" => $params ], true);
    }

    /**
     * 上传素材文件
     * @param string $accessToken
     * @param string $filepath
     */
    public static function uploadMedia(string $accessToken, string $filepath)
    {
        $url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=" . $accessToken . "&type=image";

        $client = new Client();
        $filestream = new Stream(fopen($filepath, "r"));
        $request = $client->request("POST", $url, [
            "multipart" => [
                [
                    "name"     => "media",
                    "contents" => $filestream,
                    "headers"  => [
                        "Content-Type" => "multipart/form-data"
                    ]
                ]
            ]
        ]);
        $body = $request->getBody();
        $response = $body->getContents();
        return json_decode($response, true);
    }
}