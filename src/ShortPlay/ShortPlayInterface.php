<?php

namespace QingrongBase\Tool\ShortPlay;

use QingrongBase\Tool\Bean\ShortPlay\PullUploadParamBean;

interface ShortPlayInterface
{
    /**
     * 拉取上传视频
     * @param PullUploadParamBean $pullUploadParamBean
     * @return mixed
     */
    public function pullUpload(PullUploadParamBean $pullUploadParamBean);

    /**
     * 查询视频上传状态
     * @param $taskId
     * @param $accessToken
     * @return mixed
     */
    public function getUploadStatus($taskId,$accessToken);

}