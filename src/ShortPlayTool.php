<?php

namespace QingrongBase\Tool;

use QingrongBase\Tool\Bean\ShortPlay\PullUploadParamBean;
use QingrongBase\Tool\ShortPlay\HuoshanShortPlay;
use QingrongBase\Tool\ShortPlay\WechatShortPlay;

class ShortPlayTool
{
    static $instance = [];

    /**
     * 获取对象
     * @param $type
     * @return mixed|HuoshanShortPlay|WechatShortPlay
     * @throws \Exception
     */
    public static function getInstance($type)
    {
        if (isset(self::$instance[$type])){
            return self::$instance[$type];
        }
        switch ($type){
            case "wechat":
                $obj = new WechatShortPlay();
                break;
            case "huoshan":
                $obj = new HuoshanShortPlay();
                break;
            default:
                throw new \Exception("视频类型不存在",500);
        }
        self::$instance[$type] = $obj;
        return $obj;
    }

    /**
     * 上传视频
     * @param PullUploadParamBean $pullUploadParamBean
     * @param $type
     * @return mixed|void
     * @throws \Exception
     */
    public static function pullUpload(PullUploadParamBean $pullUploadParamBean, $type = "wechat")
    {
        $instance = self::getInstance($type);
        return $instance->pullUpload($pullUploadParamBean);
    }

    /**
     * 查询任务状态
     * @param $taskId 任务ID
     * @param $accessToken token
     * @param $type
     * @return mixed|void
     * @throws \Exception
     */
    public static function getUploadStatus($taskId, $accessToken, $type = "wechat")
    {
        $instance = self::getInstance($type);
        return $instance->getUploadStatus($taskId,$accessToken);
    }
}